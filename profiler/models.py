from django.db import models
import datetime

# Create your models here.
class Case(models.Model):
    title = models.CharField(max_length=255, default="Case")
    description = models.TextField(default="Hello")
    creationDate = models.DateField(default=datetime.date.today())

class Job(models.Model):
    name = models.CharField(max_length=255)
    salary = models.IntegerField()

class Profile(models.Model):
    firstname = models.CharField(max_length=255)
    lastname = models.CharField(max_length=255)
    address = models.TextField()
    age = models.IntegerField()
    birthdate = models.DateField()
    cases = models.ManyToManyField(Case)
    description = models.TextField(blank=True)
    job = models.OneToOneField(Job, on_delete=models.CASCADE, null=True)
    
class SocialMediaAccount(models.Model):
    platform = models.CharField(max_length=255)
    url = models.TextField()
    username = models.CharField(max_length=255)
    password = models.TextField()
    creationDate = models.DateField()
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)

