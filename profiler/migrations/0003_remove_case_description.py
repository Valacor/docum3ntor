# Generated by Django 4.2.1 on 2023-05-28 20:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiler', '0002_rename_cases_case'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='case',
            name='description',
        ),
    ]
