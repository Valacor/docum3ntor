from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("<int:caseId>/", views.displayProfiles, name="displayProfiles"),
    path("create-case/", views.createCase, name="createCase"),
    path("profile/<int:profileId>", views.displayProfile, name="displayProfile")
]