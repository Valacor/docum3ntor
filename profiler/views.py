from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from .models import Case, Profile, SocialMediaAccount

# Create your views here.
def index(request):
    template = loader.get_template("profiler/index.html")
    cases = Case.objects.all()
    context = {
        "cases": [{"id": i.id, "title": i.title, "description": i.description, "creationDate": i.creationDate} for i in cases]
    }
    return HttpResponse(template.render(context, request))

def displayProfiles(request, caseId):
    case = Case.objects.get(id=caseId)
    profiles = Profile.objects.filter(cases=case)
    context = {
        "profiles": [
            {
                "id": p.id,
                "firstname": p.firstname, 
                "lastname": p.lastname,
                "age": p.age,
                "address": p.address} for p in profiles]
    }
    template = loader.get_template("profiler/profile.html")
    return HttpResponse(template.render(context, request))

def displayProfile(request, profileId): 
    if request.method == "POST":
        pass

    profile = Profile.objects.get(id=profileId)
    socialMedia = SocialMediaAccount.objects.filter(profile=profile)
    context = {
        "profile": {
            "id": profile.id,
            "firstname": profile.firstname,
            "lastname": profile.lastname,
            "age": profile.age,
            "address": profile.address,
            "birthday": profile.birthdate,
        },
        "socialMedia": [
            {
                "platform": sma.platform,
                "url": sma.url,
                "username": sma.username,
                "password": sma.password,
                "creationDate": sma.creationDate,
                 
            } for sma in socialMedia
        ]
    }

    template = loader.get_template("profiler/displayProfile.html")
    return HttpResponse(template.render(context, request))

def createCase(request):
    if request.method == "POST":
        return HttpResponse("<h1>Created</h1>")
    
    template = loader.get_template("profiler/editCase.html")
    context = {
        "case": None
    }
    return HttpResponse(template.render(context, request))
